package com.ucbcba.blog.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by amolina on 26/09/17.
 */
@Entity
public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    private String text;

    @NotNull
    @Column(columnDefinition="int(5) default '0'")
    private Integer likes = 0 ;

    @NotNull
    @ManyToOne
    @JoinColumn(name="post_id")
    private Post post;


    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    public Comment(){

    }
    public Comment(Post post){
        this.post = post;

    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
